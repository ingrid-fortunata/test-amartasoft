import initialData from "./mockData";
import { useSelector, useDispatch } from "react-redux";
import { setData } from "./app/reducer";

function App() {
  const dispatch = useDispatch();

  const newData = initialData.map((arr, index) => {
    return arr.map((obj, index2) => {
      if (index2 > 0) {
        return arr[index2 - 1] === obj
          ? "before"
          : arr[index2 - 1] === "before"
          ? "before"
          : obj;
      } else {
        return "Baris" + (index + 1);
      }
    });
  });

  const data = useSelector((state) => state.checkData.value);
  console.log("data", data);

  return (
    <button onClick={() => dispatch(setData(newData))}>Store to redux</button>
  );
}

export default App;
