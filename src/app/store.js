import { configureStore } from "@reduxjs/toolkit";
import checkData from "./reducer";

export default configureStore({
  reducer: {
    checkData: checkData,
  },
});
