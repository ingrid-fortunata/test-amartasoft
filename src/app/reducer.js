import { createSlice } from "@reduxjs/toolkit";

export const checkData = createSlice({
  initialState: {
    value: null,
  },
  name: "checkData",
  reducers: {
    setData: (state, action) => {
      //   console.log("action", action.payload);
      return {
        value: action.payload,
      };
    },
  },
});

export const { setData } = checkData.actions;

export default checkData.reducer;
